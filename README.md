You will need:

* A Local nomad client: https://www.nomadproject.io/downloads.html
* Vagrant and the Vagrant-Berkshelf plugin : http://www.vagrantup.com and https://github.com/berkshelf/vagrant-berkshelf
