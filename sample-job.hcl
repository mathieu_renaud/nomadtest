job "swarmMathieu" {
    datacenters = [ "dc1" ]
    
    type = "service"
    name = "example1"
    region = "global"
    group "swarmMathieuTG" {
        task "envServer" {
            driver = "raw_exec"
            config {
              command = "/usr/bin/python"
              args = [ "testserver.py" ]
            }
            
            artifact {
              source = "http://10.10.10.100/packages/testserver.tar.gz"
            }

            service "swarm-mathieu" {
                  check {
                      timeout = 2000000000
                      interval = 10000000000
                      protocol = ""
                      path = ""
                      type = "tcp"
                      name = "alive"
                    }
                  port = "http"
                  tags = [ "global","swarm" ]
                }
                
            env {
              SWARM_INSTANCE = "mathieu"
            }
            resources {
              network {
                  port "http" {}
                  mbits = 1
              }
              iops = 0
              disk = 500
              memory = 192
              cpu = 500
            }
            
            meta {
              foo = "bar"
              baz = "pipe"
            }
          }
        count = 3
      }
}