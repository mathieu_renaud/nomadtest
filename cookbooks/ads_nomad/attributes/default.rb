default['ads_nomad']['network_interface'] = "eth0"
default['ads_nomad']['bootstrap_expect'] = 3
default['ads_nomad']['servers'] = []
default['ads_nomad']['bind_addr'] = node['ipaddress']