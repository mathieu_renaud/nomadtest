
include_recipe "ads_nomad::default"

#SERVER

node.override['nomad']['server_enabled'] = true
node.override['nomad']['bootstrap_expect'] = node['ads_nomad']['bootstrap_expect']
node.override['nomad']['retry_join'] = node['ads_nomad']['servers']
nomad = node['nomad']

#{"enable_syslog":true,"bind_addr":"192.168.88.10","server":{"enabled":true,"bootstrap_expect":1}}
nomad_server_config '00-default' do
  enabled nomad['server_enabled']
  bootstrap_expect nomad['bootstrap_expect']
  retry_join nomad['retry_join']
end