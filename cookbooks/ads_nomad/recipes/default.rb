#
# Cookbook Name:: ads_nomad
# Recipe:: default
#
# Copyright (C) 2016 Automotive Data Solutions
#
# All rights reserved - Do Not Redistribute
#

include_recipe "nomad::install"

#ALL
node.override['nomad']['data_dir'] = '/var/lib/nomad'
node.override['nomad']['log_level'] = "WARN"
node.override['nomad']['bind_addr'] = node['ads_nomad']['bind_addr']
node.override['nomad']['enable_syslog'] = true

nomad = node['nomad']

nomad_config '00-default' do
  data_dir nomad['data_dir']
  log_level nomad['log_level']
  enable_syslog nomad['enable_syslog']
  bind_addr nomad['bind_addr']
end

include_recipe "nomad::manage"