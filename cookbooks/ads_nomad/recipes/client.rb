include_recipe "ads_nomad::default"

#CLIENT
node.override['nomad']['servers'] = node['ads_nomad']['servers']
node.override['nomad']['options']['driver.raw_exec.enable'] = 1

node.override['nomad']['network_interface'] =  node['ads_nomad']['network_interface']


options = {"driver.raw_exec.enable"=>node['nomad']['options']['driver.raw_exec.enable']}

if !(node['consul'].nil?) then
    consul_ip = node['consul']['config']['bind_addr']
    node.override['nomad']['options']['consul.address'] = "#{consul_ip}:8500"
    options["consul.address"] = node['nomad']['options']['consul.address']
end
nomad = node['nomad']

# {"client":{"enabled":true,"servers":["192.168.88.10"], "network_interface":"eth1", "options":{"consul.address":"192.168.88.10:8500","driver.raw_exec.enable":"1"}}}

nomad_client_config '00-default' do
  enabled nomad['client_enabled']
  servers nomad['servers']
  network_interface nomad['network_interface']
  options options
end