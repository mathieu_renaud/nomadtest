name             'ads_nomad'
maintainer       'Automotive Data Solutions'
maintainer_email 'mathieu.renaud@adsdata.ca'
license          'All rights reserved'
description      'Installs/Configures ads_nomad'
long_description 'Installs/Configures ads_nomad'
version          '0.1.0'

depends "nomad"
