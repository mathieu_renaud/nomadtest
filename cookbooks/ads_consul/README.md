# ads_consul-cookbook

TODO: Enter the cookbook description here.

## Supported Platforms

TODO: List your supported platforms.

## Attributes

<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['ads_consul']['bacon']</tt></td>
    <td>Boolean</td>
    <td>whether to include bacon</td>
    <td><tt>true</tt></td>
  </tr>
</table>

## Usage

### ads_consul::default

Include `ads_consul` in your node's `run_list`:

```json
{
  "run_list": [
    "recipe[ads_consul::default]"
  ]
}
```

## License and Authors

Author:: Automotive Data Solutions (<mathieu.renaud@adsdata.ca>)
