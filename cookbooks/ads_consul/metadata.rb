name             'ads_consul'
maintainer       'Automotive Data Solutions'
maintainer_email 'mathieu.renaud@adsdata.ca'
license          'All rights reserved'
description      'Installs/Configures ads_consul'
long_description 'Installs/Configures ads_consul'
version          '0.1.0'

depends "consul"