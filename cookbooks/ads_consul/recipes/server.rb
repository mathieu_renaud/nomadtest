#
# Cookbook Name:: ads_consul
# Recipe:: default
#
# Copyright (C) 2016 Automotive Data Solutions
#
# All rights reserved - Do Not Redistribute
#

# "consul" => {"config"=> {"server" => (i == 0), "bind_addr" => "192.168.88.1#{i}", "advertise_addr" => "192.168.88.1#{i}", "client_addr" => "192.168.88.1#{i}", "ui" => (true if i == 0), "bootstrap_expect" => (1 if i == 0)}}

node.override['consul']['config']['server'] = true
node.override['consul']['config']["bootstrap_expect"] = node['ads_consul']["bootstrap_expect"]

node.override['consul']['config']["bind_addr"] = node['ads_consul']['ipaddress']
node.override['consul']['config']["advertise_addr"] = node['ads_consul']['ipaddress']
node.override['consul']['config']["client_addr"] = node['ads_consul']['ipaddress']
node.override['consul']['config']['ui'] = node['ads_consul']['enable_ui']
node.override['consul']['config']["retry_join"] = node['ads_consul']['servers']


include_recipe "ads_consul::prepare"
include_recipe "consul::default"