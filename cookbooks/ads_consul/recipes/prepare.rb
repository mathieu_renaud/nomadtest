file "/var/log/consul.log" do
    mode '0777'
    action :create_if_missing
end

directory "/var/lib/consul" do
    mode '0777'
    action :create
end