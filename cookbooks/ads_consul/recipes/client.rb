#
# Cookbook Name:: ads_consul
# Recipe:: default
#
# Copyright (C) 2016 Automotive Data Solutions
#
# All rights reserved - Do Not Redistribute
#

node.override['consul']['config']["bind_addr"] = node['ads_consul']['ipaddress']
node.override['consul']['config']["advertise_addr"] = node['ads_consul']['ipaddress']
node.override['consul']['config']["client_addr"] = node['ads_consul']['ipaddress']
node.override['consul']['config']['ui'] = node['ads_consul']['enable_ui']
node.override['consul']['config']["retry_join"] = node['ads_consul']['servers']


include_recipe "ads_consul::prepare"
include_recipe "consul::default"