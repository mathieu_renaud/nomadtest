name             'ads_java'
maintainer       'Automotive Data Solutions'
maintainer_email 'mathieu.renaud@adsdata.ca'
license          'All rights reserved'
description      'Installs/Configures ads_java'
long_description 'Installs/Configures ads_java'
version          '0.1.0'

depends "java"