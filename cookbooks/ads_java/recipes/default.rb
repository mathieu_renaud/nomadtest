#
# Cookbook Name:: ads_java
# Recipe:: default
#
# Copyright (C) 2016 Automotive Data Solutions
#
# All rights reserved - Do Not Redistribute
#

node.set["java"]["install_flavor"] = "oracle"
node.set["java"]["jdk_version"] = 8
node.set["java"]['oracle']['accept_oracle_download_terms'] = true

include_recipe "java::default"