# ads_docker-cookbook

TODO: Enter the cookbook description here.

## Supported Platforms

TODO: List your supported platforms.

## Attributes

<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['ads_docker']['bacon']</tt></td>
    <td>Boolean</td>
    <td>whether to include bacon</td>
    <td><tt>true</tt></td>
  </tr>
</table>

## Usage

### ads_docker::default

Include `ads_docker` in your node's `run_list`:

```json
{
  "run_list": [
    "recipe[ads_docker::default]"
  ]
}
```

## License and Authors

Author:: Automotive Data Solutions (<mathieu.renaud@adsdata.ca>)
