#
# Cookbook Name:: ads_docker
# Recipe:: default
#
# Copyright (C) 2016 Automotive Data Solutions
#
# All rights reserved - Do Not Redistribute
#

docker_installation 'default' do
  action :create
end

docker_service_manager 'default' do
  action :start
end