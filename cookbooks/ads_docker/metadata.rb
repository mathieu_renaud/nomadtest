name             'ads_docker'
maintainer       'Automotive Data Solutions'
maintainer_email 'mathieu.renaud@adsdata.ca'
license          'All rights reserved'
description      'Installs/Configures ads_docker'
long_description 'Installs/Configures ads_docker'
version          '0.1.0'
depends "docker"
