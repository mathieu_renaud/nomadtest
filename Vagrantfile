# -*- mode: ruby -*-
# vi: set ft=ruby :

ChefRepoPath = "."

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(2) do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "bento/centos-6.7"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
     vb.memory = "1024"
  end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   sudo apt-get update
  #   sudo apt-get install -y apache2
  # SHELL
  
  config.berkshelf.enabled = true
  
  config.vm.define "nomadserver0" do |web|
       web.vm.hostname = "nomadserver0"
       web.vm.network "private_network", ip: "192.168.90.10"
       web.vm.provision :chef_zero do |chef|
         # Allow Chef output config via command line like this:
         # CHEF_FORMAT=doc CHEF_LOG=warn vagrant up
         chef.formatter = ENV.fetch("CHEF_FORMAT", "null").downcase.to_sym
         chef.log_level = ENV.fetch("CHEF_LOG", "info").downcase.to_sym

         # chef.cookbooks_path = "#{ChefRepoPath}/cookbooks"
         chef.roles_path = "#{ChefRepoPath}/roles"
         chef.data_bags_path = "#{ChefRepoPath}/data_bags"
         chef.environments_path = "#{ChefRepoPath}/environments"
	 chef.nodes_path = "#{ChefRepoPath}/nodes"
         chef.environment = "development"

         chef.add_recipe "ads_consul::server"
         chef.add_recipe "ads_nomad::server"   
     
         chef.json = {
            "ads_nomad"=>{"bind_addr"=>"192.168.90.10", "servers"=>[0].map! {|x| "192.168.90.1#{x}" }, "network_interface"=>"eth1", "bootstrap_expect" => 1},
            "ads_consul"=>{"ipaddress"=>"192.168.90.10", "servers"=>[0].map! {|x| "192.168.90.1#{x}" }, "enable_ui" => true, "bootstrap_expect" => 1}
         }
      end
  end
  (1..3).to_a.each do |i|
     config.vm.define "nomad#{i}" do |web|
       web.vm.hostname = "nomad#{i}"
       web.vm.network "private_network", ip: "192.168.90.1#{i}"
       web.vm.provision :chef_zero do |chef|
         # Allow Chef output config via command line like this:
         # CHEF_FORMAT=doc CHEF_LOG=warn vagrant up
         chef.formatter = ENV.fetch("CHEF_FORMAT", "null").downcase.to_sym
         chef.log_level = ENV.fetch("CHEF_LOG", "info").downcase.to_sym

         # chef.cookbooks_path = "#{ChefRepoPath}/cookbooks"
         chef.roles_path = "#{ChefRepoPath}/roles"
         chef.data_bags_path = "#{ChefRepoPath}/data_bags"
         chef.environments_path = "#{ChefRepoPath}/environments"
         chef.nodes_path = "#{ChefRepoPath}/nodes"
         chef.environment = "development"

         chef.add_recipe "ads_docker"
         chef.add_recipe "ads_java"
         chef.add_recipe "ads_consul::client"
         chef.add_recipe "ads_nomad::client" 
     
         chef.json = {
            "ads_nomad"=>{"bind_addr"=>"192.168.90.1#{i}", "servers"=>[0].map! {|x| "192.168.90.1#{x}" }, "network_interface"=>"eth1"},
            "ads_consul"=>{"ipaddress"=>"192.168.90.1#{i}", "servers"=>[0].map! {|x| "192.168.90.1#{x}" }, "enable_ui" => false}
         }
      end
    end
  end
end

