import os, json
import BaseHTTPServer

addr="0.0.0.0"
port=8080

addressPort = os.getenv("NOMAD_ADDR_http")

(addr,port) = tuple(addressPort.split(":"))

port = int(port)
foo = os.getenv("foo")

class MyHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200,"GET!")
        self.send_header("Content-type", "application/json")
        self.end_headers()
        self.wfile.write(json.dumps(dict(os.environ)))
    def do_POST(self):
        print "POST!"


server = BaseHTTPServer.HTTPServer((addr,port), MyHandler)

server.serve_forever()
