job "touchMathieu" {
    datacenters = [ "dc1" ]
    
    type = "batch"
    name = "example1"
    region = "global"
    group "touchMathieuTG" {
        task "touch" {
            driver = "raw_exec"
            config {
              command = "touch"
              args = [ "/tmp/touchme" ]
            }
            
            env {
              SWARM_INSTANCE = "mathieu"
            }
            resources {
              iops = 0
              disk = 500
              memory = 192
              cpu = 500
            }
            
            meta {
              foo = "bar"
              baz = "pipe"
            }
          }
        count = 1
      }

    periodic {
        // Launch every 15 minutes
        cron = "*/2 * * * * *"

        // Do not allow overlapping runs.
        prohibit_overlap = true
    }
}
